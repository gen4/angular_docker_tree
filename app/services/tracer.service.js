'use strict';
/**
 * @ngdoc service
 * @name admin.dataLayer
 * @description
 * # dataLayer
 * Service in the admin.
 */
angular.module('demoapp')
	.service('tracerService', ['$http', function ($http) {
		//here $http just for example
		this.list=[];
		this.addItem = function(label){
			this.list.push({label:label,index:this.list.length,time:new Date().getTime()});
			console.log('tracer!',this.list);
		}
	}]);
