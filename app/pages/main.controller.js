angular.module('demoapp').controller('MainCtrl', ['$scope', '$state','tracerService', function($scope, $state,tracerService) {

	$scope.tricedItems = tracerService.list;
	$scope.gotoTracer = function(){
		$state.go('tracer');
	}
	$scope.remove = function (scope) {
		tracerService.addItem('remove item');
		scope.remove();
	};

	$scope.toggle = function (scope) {
		tracerService.addItem('toggle');
		scope.toggle();
	};

	$scope.moveLastToTheBeginning = function () {
		tracerService.addItem('moveLastToTheBeginning');
		var a = $scope.data.pop();
		$scope.data.splice(0, 0, a);
	};

	$scope.newSubItem = function (scope) {
		tracerService.addItem('new subItem');
		var nodeData = scope.$modelValue;
		nodeData.nodes.push({
			id: nodeData.id * 10 + nodeData.nodes.length,
			title: nodeData.title + '.' + (nodeData.nodes.length + 1),
			nodes: []
		});
	};

	$scope.collapseAll = function () {
		tracerService.addItem('collapseAll');
		$scope.$broadcast('angular-ui-tree:collapse-all');
	};

	$scope.expandAll = function () {
		tracerService.addItem('expandAll');
		$scope.$broadcast('angular-ui-tree:expand-all');
	};

	$scope.data = [{
		'id': 1,
		'title': 'node1',
		'nodes': [
			{
				'id': 11,
				'title': 'node1.1',
				'nodes': [
					{
						'id': 111,
						'title': 'node1.1.1',
						'nodes': []
					}
				]
			},
			{
				'id': 12,
				'title': 'node1.2',
				'nodes': []
			}
		]
	}, {
		'id': 2,
		'title': 'node2',
		'nodrop': true, // An arbitrary property to check in custom template for nodrop-enabled
		'nodes': [
			{
				'id': 21,
				'title': 'node2.1',
				'nodes': []
			},
			{
				'id': 22,
				'title': 'node2.2',
				'nodes': []
			}
		]
	}, {
		'id': 3,
		'title': 'node3',
		'nodes': [
			{
				'id': 31,
				'title': 'node3.1',
				'nodes': []
			}
		]
	}];

}]);
